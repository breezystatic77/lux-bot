const db = require('./db');

class BotConfig {
	constructor(client) {
		db.create('lux_config')
			.then(res => {
				console.log("config db doesn't exist, creating with default values...");
				db.use('lux_config').insert({
					prefix: ']',
					channel_whitelist: [],
					channel_blacklist: []
				}, 'CONFIG_MASTER');
			})
			.catch(res => { /* catch config db lux already exists */ });
		this.client = client;
		this.configdb = db.use('lux_config');
	}

	setItem(key, value) {
		let data = {};
		data[key] = value;
		return this.configdb.insert(data, 'CONFIG_MASTER')
			.then(body => {
				return body;
			});
	}

	getItem(key) {
		return this.configdb.get('CONFIG_MASTER')
			.then(body => {
				return body[key];
			});
	}

	getAll() {
		return this.configdb.get('CONFIG_MASTER').then(body => { return body });
	}
}

module.exports = (client) => {
	return new BotConfig(client);
}