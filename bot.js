require('dotenv').config();
const Discord = require('discord.js');

const client = new Discord.Client();

const botconfig = require('./botconfig')(client);
const commandStrings = require('./commands.json');

let commands = {};
commandStrings.forEach(cmdObj => {
	commands[cmdObj.command] = require('./commands/' + cmdObj.command);
	commands[cmdObj.command].client = client;
});

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}`);
	client.user.setPresence({
		game: {
			name: 'running errands on Easy Street...',
		}
	});
});

client.on('message', msg => {
	handleMessage(msg);
});

async function handleMessage(msg) {
	const prefix = await botconfig.getItem('prefix');
	if (msg.content.charAt(0) === prefix) {
		let args = msg.content.substring(1).split(' ');
		console.log("DOING COMMAND", args[0], "WITH ARGS", args.slice(1));
		if (args[0] in commands) {
			commands[args[0]].do(msg, args.slice(1));
		} else {
			msg.reply(`Unrecognized command: '${args[0]}'`);
		}
	}
}

botconfig.getAll().then(res => console.log(res));
botconfig.getItem('prefix').then(res => console.log(res));

client.login(process.env.BOT_TOKEN);