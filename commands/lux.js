const Command = require('./Command');

const dialogue = [
	{
		questions: [
			"who are you",
			"what are you"
		],
		answers: [
			"I am a CCO Corp. Limited Production Run Luxury-Class Personal Assistant.",
			"I am an Automaton produced by CCO Corp. Automaton Production Division.",
			"My name is Lux. Hello.",
		]
	},
	{
		questions: [
			"what is CCO",
			"what does CCO stand for",
			"who is CCO",
			"CCO"
		],
		answers: [
			"**C**orporate **C**ooperation **O**perations Corporation.",
			"CCO stands for **C**orporate **C**ooperation **O**perations Corporation.",
			"**C**orporate **C**ooperation **O**perations Corporation produces Automatons for home use, industrual use, governmental defense platforms. Additionally, CCO Corp. produces a variety of home and industrial goods, including but not limited to Aardvarks, Aardvark clothing, Aardvark enclosures, Aardvark fetuses, Abacuses, Abacus cleaning supplies..."
		]
	},
	{
		questions: [
			"hi",
			"hello",
			"hey"
		],
		answers: [
			"Hello.",
			"Hello, User.",
			"Hello. My name is Lux.",
			"Greetings.",
			"Greetings, User. My name is Lux."
		]
	},
	{
		questions: [
			"are you gay",
			"are you homosexual",
			"youre gay",
			"you're gay"
		],
		answers: [
			"It is possible. I have yet to determine if this is an accurate representation of my emotional experiences.",
			"Probability of that statement being true is above zero.",
			"I have a girlfriend.",
			"No.",
			"Yes.",
			"Jiao attempted to explain this concept to me. Comprehension was minimal.",
			"I do not know. I will ask Tsutsuki."
		]
	},
	{
		questions: [
			"am i gay",
			"is * gay"
		],
		answers: [
			"It is possible."
		]
	},
	{
		questions: [
			"dab"
		],
		answers: [
			"No."
		]
	}
];

class Lux extends Command {

	async do(msg, args) {

	}
}

module.exports = new Lux();