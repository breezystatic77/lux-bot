const Command = require('./Command');
const pkg = require('../package.json');

class Version extends Command {

	do(msg, args) {
		this.createReply(msg, `Running Lux version ${pkg.version} ✅`);
	}
}

module.exports = new Version();