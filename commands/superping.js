const Command = require('./Command');

class Superping extends Command {
	constructor() {
		super();
	}

	do(msg, args) {
		const pingMsg = args[0] !== undefined ? args[0] : "ping";
		msg.reply(`✨✨✨🎉🎉 ${pingMsg} 🎉🎉✨✨✨`)
			.then(sent => sent.delete(3 * 1000))
			.then(_ => msg.react('🎉'));
	}
}

module.exports = new Superping();