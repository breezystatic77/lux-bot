const SELF_DESTRUCT_TIME = 3 * 1000;
class Command {
	constructor() {
		this.client = null;
	}

	do(msg, args) {
		msg.reply("This command hasn't been implemented yet! Complain to suel.");
	}

	selfDestructReply(reply, emoji) {
		reply.repliedTo.react(emoji);
		reply.delete(SELF_DESTRUCT_TIME).then(_ => {
			// reply.repliedTo.react(emoji);
		});
	}

	async createReply(msg, content, sdEmoji = '') {
		let reply = null;
		await msg.reply(content).then(r => reply = r);
		reply.repliedTo = msg;
		if (sdEmoji != '') {
			this.selfDestructReply(reply, sdEmoji);
		}
		return reply;
	}

	async editReply(reply, content, sdEmoji = '') {
		reply.edit(`<@${reply.repliedTo.author.id}>, ${content}`);
		if (sdEmoji != '') {
			this.selfDestructReply(reply, sdEmoji);
		}
	}

	doArgWithData(msg, args, cb) {
		if (args.length >= 2) {
			const data = args.slice(1).join(' ');
			// console.log(data);
			cb(data);
		} else {
			msg.reply("Not enough arguments.");
		}
	}

	async replyMustBeOwner(msg) {
		this.createReply(msg, 'You must be the server owner to perform this action.');
	}

	async replyMustBeOperator(msg) {
		this.createReply(msg, 'You must be a Lux operator to perform this action.');
	}
}

module.exports = Command;