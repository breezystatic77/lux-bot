const Command = require('./Command');

const commands = require('../commands.json');

class Help extends Command {
	constructor() {
		super();
	}

	do(msg, args) {
		const cmdlist = commands.map(cmd => cmd.command);
		if (args.length == 0) {
			let newlineList = cmdlist.map(cmd => {
				return cmd + "\n";
			}).join('');
			msg.reply("\`\`\`" + newlineList + "\`\`\`");
		} else {
			if (cmdlist.includes(args[0])) {
				//const helpString = require('./' + args[0]).help;
				const c = commands.find(cmd => cmd.command === args[0]);
				msg.reply(`\`${c.command}\`\n\`\`\`markdown\n${c.help}\`\`\``);
			} else {
				msg.reply("that command doesn't exist.");
			}
		}
	}
}

module.exports = new Help();