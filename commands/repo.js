const Command = require('./Command');
const pkg = require('../package.json');

class Repo extends Command {
	do(msg, args) {
		this.createReply(msg, `${pkg.homepage}`);
	}
}

module.exports = new Repo();
