const Command = require('./Command');
const nano = require('nano')('http://localhost:5984/');
const AsciiTable = require('ascii-table');
const moment = require('moment');
const ellipsis = require('text-ellipsis');


const MAX_TITLE_LENGTH = 30;

class Movies extends Command {
	constructor() {
		super();

		nano.db.create('lux_movies')
			.then(res => {
				console.log("Creating movies database...");
			}).catch(_ => { }); // disable error
		this.movies = nano.db.use('lux_movies');
	}

	do(msg, args) {
		if (args.length == 0 || args[0] == "list") {
			this.listMovies(msg);
		} else {
			if (args[0] == "add") {
				this.doArgWithData(msg, args, data => {
					this.addMovie(msg, args, data);
				});
			} else if (args[0] == "remove") {
				this.doArgWithData(msg, args, data => {
					this.removeMovie(msg, args, data);
				});
			} else if (args[0] == "random") {
				this.randomMovie(msg);
			}
		}
	}

	async listMovies(msg) {
		const reply = await this.createReply(msg, 'loading table...');
		const table = new AsciiTable();
		const movieList = await this.movies.list({ include_docs: true })
			.then(body => body.rows.map(row => {

				this.addMovieToTable(table, row.doc);
			}));
		this.editReply(reply, `\`\`\`${table.toString()}\`\`\``);
	}

	async addMovie(msg, args, data) {
		let reply = await this.createReply(msg, 'adding your movie...');
		const movieTitle = data.substring(0, MAX_TITLE_LENGTH);
		this.movies.insert({
			adder: msg.author.id,
			adder_name: msg.author.tag,
			added: Date.now()
		}, movieTitle)
			.then(res => {
				this.editReply(reply, `added movie: \`${movieTitle}\``, '✅');
			})
			.catch(res => {
				this.movies.get(movieTitle).then(mv => {
					this.editReply(reply, `movie already added by: @${mv.adder_name}`, '❌');
				}).catch(mv => {
					this.editReply(reply, `unknown error.`);
				});

			});
	}

	async removeMovie(msg, args, data) {
		let reply = await this.createReply(msg, 'removing movie...');
		this.movies.get(data).then(body => {
			if (msg.author.id == body.adder) {
				this.movies.destroy(data, body._rev).then(_ => {
					this.editReply(reply, `movie removed from list.`, '✅');
				});
			} else {
				this.editReply(reply, `movie not removed: You didn't add this movie.`, '❌');
			}
		}).catch(res => {
			this.editReply(reply, `error: no movie with that name found.`);
		})
	}

	async randomMovie(msg) {
		let reply = await this.createReply(msg, 'loading random movie...');

		this.movies.list({ include_docs: true })
			.then(body => {
				const movieList = body.rows;
				const randomMovie = movieList[Math.floor(Math.random() * movieList.length)].doc;
				const table = new AsciiTable();
				this.addMovieToTable(table, randomMovie);
				this.editReply(reply, `\`\`\`${table.toString()}\`\`\``);
			});
	}

	addMovieToTable(table, doc) {
		const t = moment(doc.added).fromNow();
		table.addRow(doc._id, doc.adder_name, t);
	}
}

module.exports = new Movies();