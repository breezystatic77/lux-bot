const Command = require('./Command');

class Ping extends Command {
	constructor() {
		super();
	}

	do(msg, args) {
		msg.reply('pong');
	}

}

module.exports = new Ping();