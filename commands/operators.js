const Command = require('./Command');
const db = require('../db');
const AsciiTable = require('ascii-table');

class Operators extends Command {

	constructor() {
		super();

		nano.db.create('lux_operators')
			.then(res => {
				console.log("Creating operators database...");
			}).catch(_ => { }); // disable error
		this.operators = nano.db.use('lux_operators');
	}

	async do(msg, args) {
		const ownerID = msg.guild.ownerID
		const authorID = msg.author.id;
		if(args.length == 0 || args[0] == "list") {
			this.listOperators(msg);
		}
		else if (ownerID == authorID) {
			if(args[0] == 'add') {
				this.addOperators(msg);
			} else if (args[0] == 'remove') {
				this.removeOperators(msg);
			}
		} else {
			this.replyMustBeOwner(msg);
		}
	}

	async listOperators(msg) {
		const reply = await this.createReply(msg, 'loading table...');
		const table = new AsciiTable();
		await this.operators.list({ include_docs: true })
			.then(body > body.rows.map(row => {
				this.addOperatorToTable(table, row.doc);
			}));
		this.editReply(reply, `\`\`\`${table.toString()}\`\`\``);
	}

	async addOperators(msg) {
		const users = msg.mentions.users;
	}

	async removeOperators(msg) {
		const users = msg.mentions.users;
	}

	addOperatorToTable(table, doc) {

	}
}